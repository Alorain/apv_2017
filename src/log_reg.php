<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/login', function (Request $request, Response $response, $args) {
    // zobrazí přihlašovací form
    return $this->view->render($response, 'login.latte');
})->setName('login');

$app->post('/login', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    //print_r($data);
    try {
        $stmt = $this->db->prepare('SELECT * FROM account WHERE login = :l');
        $stmt->bindValue(':l', $data['log']);
        $stmt->execute();
        $acc = $stmt->fetch();
        if ($acc) {
            //print_r($acc);
            if (password_verify($data['pass'], $acc['password'])) {
                $_SESSION['user'] = $acc;
                return $response->withHeader('Location', $this->router->pathFor('index'));
            }
        }
        return $response->withHeader('Location', $this->router->pathFor('login'));
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
});

$app->get('/register', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'register.latte');
})->setName('register');

$app->post('/register', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if ($data['pass'] == $data['conf']) {
        try {
            $stmt = $this->db->prepare('SELECT * FROM account WHERE login = :l');
            $stmt->bindValue(':l', $data['log']);
            $stmt->execute();
            $acc = $stmt->fetch();
            if (!$acc) {
                $hash = password_hash($data['pass'], PASSWORD_DEFAULT);
                $stmt = $this->db->prepare('INSERT INTO account (login, password) VALUES (:l, :p)');
                $stmt->bindValue(':l', $data['log']);
                $stmt->bindValue(':p', $hash);
                $stmt->execute();
                $tplVars['error'] = "Účet úspěšně vytvořen.";
                return $response->withHeader('Location', $this->router->pathFor('login'));
            } else {
                $tplVars['error'] = "Účet již existuje!";
                return $this->view->render($response, 'register.latte', $tplVars);
            }
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            exit($ex->getMessage());
        }
    } else {
        $tplVars['error'] = "Hesla se neschodují!";
        return $this->view->render($response, 'register.latte', $tplVars);
    }
});
