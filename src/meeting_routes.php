<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/edit_meeting', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    if (empty($id)) {
        exit('Neni zadano ID schůzky.');
    }
    try {
        $stmt = $this->db->prepare("SELECT id_meeting, concat(to_char(start, 'YYYY-MM-dd'),'T',to_char(start, 'HH24:MI:SS')) AS start, description, duration, id_location FROM meeting WHERE id_meeting = :id");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $schuzka = $stmt->fetch();
        $stmt2 = $this->db->query('SELECT * FROM location');
        $lokace = $stmt2->fetchAll();
        if (!empty($schuzka)) {
            $tplVars['schuzka'] = $schuzka;
            $tplVars['lokace'] = $lokace;
            return $this->view->render($response, 'edit_meeting.latte', $tplVars);
        } else {
            exit('Schůzka nenalezana.');
        }
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('edit_meeting');

$app->post('/edit_meeting', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    if (empty($id)) {
        exit('Neni zadano ID schůzky.');
    }
    $parametry = $request->getParsedBody();

    try {
        if (!empty($parametry['loc'])/* && !empty($parametry['start']) */) {
            $len = empty($parametry['len']) ? null : $parametry['len'];
            $desc = empty($parametry['desc']) ? "" : $parametry['desc'];
            $stmt = $this->db->prepare("UPDATE meeting SET description = :desc, duration = :len, id_location = :loc,  start = to_timestamp(:st, 'YYYY-MM-DD HH24:MI:SS')"
                    . 'WHERE id_meeting = :id');
            $stmt->bindValue(':desc', $desc);
            $stmt->bindValue(':len', $len);
            $stmt->bindValue(':loc', $parametry['loc']);
            $start = (new DateTime($parametry['start']))->format('Y-m-d G:i:s');
            $stmt->bindValue(':st', $start);
            $stmt->bindValue(':id', $id);

//            var_dump($stmt);
//            die();

            $stmt->execute();
            return $response->withHeader('Location', $this->router->pathFor('print_meetings'));
        } else {
            $tplVars['error'] = 'Zadejte povinne udaje.';
            return $this->view->render($response, 'edit_meeting.latte', $tplVars);
        }
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        //exit('Zavazna chyba aplikace');
        exit($ex->getMessage());
    }
});

$app->post('/delete_meeting', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare(
                'DELETE FROM meeting WHERE id_meeting=:id'
        );
        $stmt->bindValue(':id', $data['id']);
        $stmt->execute();
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('print_meetings'));
})->setName('delete_meeting');

$app->post('/delete_from_meeting', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare('DELETE FROM person_meeting WHERE id_person = :idP AND id_meeting = :idM');
        $stmt->bindValue(':idP', $data['idP']);
        $stmt->bindValue(':idM', $data['idM']);
        $stmt->execute();
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('print_meetings'));
})->setName('delete_from_meeting');

$app->get('/meetings', function (Request $request, Response $response, $args) {
    try {
        $stmt = $this->db->prepare('SELECT * FROM meeting
                                LEFT JOIN (
                                    SELECT id_meeting, COUNT(id_meeting) as ucastnici 
                                    FROM person_meeting 
                                    JOIN person USING (id_person)
                                    GROUP BY id_meeting) AS ucast 
                                USING (id_meeting)
                                JOIN location USING (id_location)
                                ORDER BY start
                                LIMIT 10 OFFSET :o');
        $stmt->bindValue(':o', 10 * $request->getQueryParam('offset', 0));
        $stmt->execute();
        $schuzky = $stmt->fetchAll();
        $stmt2 = $this->db->query('SELECT COUNT(*) AS pocet FROM meeting');
        $info = $stmt2->fetch();
        $tplVars['pocet'] = ceil($info['pocet'] / 10);
        $tplVars['schuzky'] = $schuzky;
        return $this->view->render($response, 'print_meetings.latte', $tplVars);
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('print_meetings');

$app->get('/meeting_party', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    if (empty($id)) {
        exit('Neni zadano ID schůzky.');
    }
    try {
        $stmt = $this->db->prepare('SELECT * FROM person_meeting
                                    JOIN person USING (id_person)
                                    WHERE id_meeting = :id
                                    ORDER BY id_person');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $skupina = $stmt->fetchAll();

        $stmt2 = $this->db->query('SELECT * FROM person ORDER BY last_name');
        $osoby = $stmt2->fetchAll();

        $stmt3 = $this->db->prepare('SELECT * FROM location WHERE id_location = :id');
        $stmt3->bindValue(':id', $id);
        $stmt3->execute();
        $adresa = $stmt3->fetch();

        $tplVars['skupina'] = $skupina;
        $tplVars['osoby'] = $osoby;
        $tplVars['adresa'] = $adresa;
        $tplVars['id'] = $id;
        return $this->view->render($response, 'meeting_party.latte', $tplVars);
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('meeting_party');

$app->post('/meeting_party', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare('INSERT INTO person_meeting (id_person, id_meeting) VALUES (:idP, :idM)');
        $stmt->bindValue(':idP', $data['idP']);
        $stmt->bindValue(':idM', $data['idM']);
        $stmt->execute();

        $stmt2 = $this->db->query('SELECT * FROM person ORDER BY last_name');
        $osoby = $stmt2->fetchAll();

        $stmt3 = $this->db->prepare('SELECT * FROM person_meeting
                                    JOIN person USING (id_person)
                                    WHERE id_meeting = :id
                                    ORDER BY id_person');
        $stmt3->bindValue(':id', $data['idM']);
        $stmt3->execute();
        $skupina = $stmt3->fetchAll();

        $stmt4 = $this->db->prepare('SELECT * FROM location WHERE id_location = :id');
        $stmt4->bindValue(':id', $id);
        $stmt4->execute();
        $adresa = $stmt4->fetch();

        $tplVars['skupina'] = $skupina;
        $tplVars['osoby'] = $osoby;
        $tplVars['id'] = $data['idM'];
        $tplVars['adresa'] = $adresa;
        return $this->view->render($response, 'meeting_party.latte', $tplVars);
    } catch (Exception $ex) {
        if ($ex->getCode() == 23505) {
            $tplVars['error'] = 'Tato osoba se schůzky už účastní.';
        } else {
            $this->logger->error($ex->getMessage());
            exit($ex->getMessage());
        }
    }
});

$app->get('/new_meeting', function (Request $request, Response $response, $args) {
    try {
        $stmt = $this->db->query('SELECT * FROM location ORDER BY id_location');
        $lokace = $stmt->fetchAll();
        $tplVars['lokace'] = $lokace;
        return $this->view->render($response, 'new_meeting.latte', $tplVars);
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('new_meeting');

$app->post('/new_meeting', function (Request $request, Response $response, $args) {
    $parametry = $request->getParsedBody();
    try {
        if (!empty($parametry['loc']) && !empty($parametry['start'])) {
            $len = empty($parametry['len']) ? null : $parametry['len'];
            $desc = empty($parametry['desc']) ? "" : $parametry['desc'];
            $stmt = $this->db->prepare("INSERT INTO meeting (description, duration, id_location, start) "
                    . "VALUES (:desc, :len, :loc, to_timestamp(:st, 'YYYY-MM-DD HH24:MI:SS'))");
            $stmt->bindValue(':desc', $desc);
            $stmt->bindValue(':len', $len);
            $stmt->bindValue(':loc', $parametry['loc']);
            $start = (new DateTime($parametry['start']))->format('Y-m-d G:i:s');
            $stmt->bindValue(':st', $start);

            $stmt->execute();
            return $response->withHeader('Location', $this->router->pathFor('print_meetings'));
        } else {
            $tplVars['error'] = 'Zadejte povinne udaje.';
            return $this->view->render($response, 'edit_meeting.latte', $tplVars);
        }
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        //exit('Zavazna chyba aplikace');
        exit($ex->getMessage());
    }
});
