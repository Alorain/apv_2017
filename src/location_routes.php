<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/bydliste', function (Request $request, Response $response, $args) {
    try {
        $tplVars['filtr'] = [
            'nick' => 'on',
            'date' => 'on',
            'height' => 'on',
            'gender' => 'on',
            'street' => 'on',
            'number' => 'on',
            'zip' => 'on',
            'radit' => 'OrPr',
            'vzestupne' => 'vzestup'
        ];
        $stmt = $this->db->query('SELECT first_name, last_name, nickname, birth_day, gender, height, city, street_name, street_number, zip FROM person '
                . 'JOIN location USING (id_location) ORDER BY last_name');
        $data = $stmt->fetchAll();
        $tplVars['udaje'] = $data;
        $this->view->render($response, 'bydliste.latte', $tplVars);
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('bydliste');

$app->post('/bydliste', function (Request $request, Response $response, $args) {
    $filtr = $request->getParsedBody();
    //    $stmt = $this->db->query('SELECT first_name, last_name, nickname, birth_day, gender, height, city, street_name, street_number, zip FROM person JOIN location ON person.id_person=location.id_location ORDER BY last_name ASC');
    try {
        $query = 'SELECT first_name, last_name, nickname, birth_day, gender, height, city, street_name, street_number, zip FROM person '
                . 'JOIN location USING (id_location) ORDER BY ';
        if ($filtr['radit'] == 'OrPr') {
            $query = $query . 'last_name ';
        } elseif ($filtr['radit'] == 'OrDn') {
            $query = $query . 'birth_day ';
        } else {
            $query = $query . 'city ';
        }

        if ($filtr['vzestupne'] == 'vzestup') {
            $query = $query . 'ASC';
        } else {
            $query = $query . 'DESC';
        }
        $stmt = $this->db->query($query);
        $data = $stmt->fetchAll();
        $tplVars['udaje'] = $data;
        $tplVars['filtr'] = $filtr;
        $this->view->render($response, 'bydliste.latte', $tplVars);
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('bydliste');

$app->get('/adresy', function (Request $request, Response $response, $args) {
    try {
        $parametry = $request->getQueryParams();
        if (empty($parametry['filtr'])) {
            $stmt = $this->db->prepare('  SELECT * FROM location AS loc
                                        LEFT JOIN(
                                            SELECT id_location, COUNT(id_person) as residenti FROM person 
                                            GROUP BY id_location
                                        ) AS res USING (id_location) ORDER BY city
                                        LIMIT 10 OFFSET :o');
            $stmt->bindValue(':o', 10 * $request->getQueryParam('offset', 0));
            $stmt->execute();
            $data = $stmt->fetchAll();
            $tplVars['adresy'] = $data;
            $stmt2 = $this->db->query('SELECT COUNT(*) AS pocet FROM location');
            $info = $stmt2->fetch();
            $tplVars['pocet'] = ceil($info['pocet'] / 10);
        } else {
            $stmt = $this->db->prepare('  SELECT * FROM location AS loc
                                        LEFT JOIN(
                                            SELECT id_location, COUNT(id_person) as residenti FROM person 
                                            GROUP BY id_location
                                        ) AS res USING (id_location) 
                                        WHERE city LIKE :para OR street_name LIKE :para OR country LIKE :para OR name LIKE :para
                                        ORDER BY city');
            $stmt->bindValue(':para', '%' . $parametry['filtr'] . '%');
            $stmt->execute();
            $data = $stmt->fetchAll();
            $tplVars['adresy'] = $data;
        }
        return $this->view->render($response, 'adresy.latte', $tplVars);
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
})->setName('adresy');

$app->get('/new_address', function (Request $request, Response $response, $args) {
    $tplVars['data'] = [
        'me' => '',
        'ul' => '',
        'cd' => '',
        'psc' => '',
        'coun' => '',
        'desc' => '',
        'lat' => '',
        'lon' => '',
    ];
    return $this->view->render($response, 'new_address.latte', $tplVars);
})->setName('new_address');

$app->post('/new_address', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        if (!empty($data)) { // me, ul, cd, psc, coun, desc, lat, lon
            $me = empty($data['me']) ? null : $data['me'];
            $ul = empty($data['ul']) ? null : $data['ul'];
            $cd = empty($data['cd']) ? null : $data['cd'];
            $psc = empty($data['psc']) ? null : $data['psc'];
            $coun = empty($data['coun']) ? null : $data['coun'];
            $desc = empty($data['desc']) ? null : $data['desc'];
            $lat = empty($data['lat']) ? null : $data['lat'];
            $lon = empty($data['lon']) ? null : $data['lon'];
            $stmt = $this->db->prepare('INSERT INTO location (city, street_name, street_number, zip, country, name, latitude, longitude) 
                                        VALUES (:me, :ul, :cd, :psc, :coun, :desc, :lat, :lon)');
            $stmt->bindValue(':me', $me);
            $stmt->bindValue(':ul', $ul);
            $stmt->bindValue(':cd', $cd);
            $stmt->bindValue(':psc', $psc);
            $stmt->bindValue(':coun', $coun);
            $stmt->bindValue(':desc', $desc);
            $stmt->bindValue(':lat', $lat);
            $stmt->bindValue(':lon', $lon);
            $stmt->execute();

            return $response->withHeader('Location', $this->router->pathFor('adresy'));
        } else {
            $tplVars['error'] = 'Zadejte minimálně 1 údaj.';
        }
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
    $tplVars['adresa'] = $data;
    return $this->view->render($response, 'new_address.latte', $tplVars);
});

$app->get('/edit_address', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try {
        $stmt = $this->db->prepare('SELECT * FROM location WHERE id_location = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $data = $stmt->fetch();
        $tplVars['data'] = $data;
        return $this->view->render($response, 'edit_address.latte', $tplVars);
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
})->setName('edit_address');

$app->post('/edit_address', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        if (!empty($data)) { // me, ul, cd, psc, coun, desc, lat, lon
            $me = empty($data['me']) ? null : $data['me'];
            $ul = empty($data['ul']) ? null : $data['ul'];
            $cd = empty($data['cd']) ? null : $data['cd'];
            $psc = empty($data['psc']) ? null : $data['psc'];
            $coun = empty($data['coun']) ? null : $data['coun'];
            $desc = empty($data['desc']) ? null : $data['desc'];
            $lat = empty($data['lat']) ? null : $data['lat'];
            $lon = empty($data['lon']) ? null : $data['lon'];
            $stmt = $this->db->prepare('UPDATE location SET city = :me, street_name = :ul, street_number = :cd, zip = :psc, country = :coun, name = :desc, 
                                        latitude = :lat, longitude = :lon
                                        WHERE id_location = :id');
            $stmt->bindValue(':me', $me);
            $stmt->bindValue(':ul', $ul);
            $stmt->bindValue(':cd', $cd);
            $stmt->bindValue(':psc', $psc);
            $stmt->bindValue(':coun', $coun);
            $stmt->bindValue(':desc', $desc);
            $stmt->bindValue(':lat', $lat);
            $stmt->bindValue(':lon', $lon);
            $stmt->bindValue(':id', $data['id']);
            $stmt->execute();

            return $response->withHeader('Location', $this->router->pathFor('adresy'));
        } else {
            $tplVars['error'] = 'Zadejte minimálně 1 údaj.';
        }
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
    $tplVars['adresa'] = $data;
    return $this->view->render($response, 'edit_address.latte', $tplVars);
});

$app->post('/delete_address', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare(
                'DELETE FROM location WHERE id_location = :id'
        );
        $stmt->bindValue(':id', $data['id']);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('adresy'));
})->setName('delete_address');

$app->get('/print_resident', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try {
        $stmt = $this->db->prepare('SELECT * FROM person WHERE id_location = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $data = $stmt->fetchAll();
        
        $stmt2 = $this->db->query('SELECT * FROM person ORDER BY last_name');
        $osoby = $stmt2->fetchAll();
        
        $tplVars['osoby'] = $osoby;
        $tplVars['res'] = $data;
        return $this->view->render($response, 'print_resident.latte', $tplVars);
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
})->setName('print_resident');

$app->post('/print_resident', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare('UPDATE person SET id_location = :idL WHERE id_person = :idP');
        $stmt->bindValue(':idL', $data['idL']);
        $stmt->bindValue(':idP', $data['idP']);
        $stmt->execute();

        $stmt2 = $this->db->prepare('SELECT * FROM person WHERE id_location = :idL');
        $stmt2->bindValue(':idL', $data['idL']);
        $stmt2->execute();
        $data = $stmt2->fetchAll();
        $tplVars['res'] = $data;
        return $this->view->render($response, 'print_resident.latte', $tplVars);
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
});

$app->post('/move_out', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare('UPDATE person SET id_location = NULL WHERE id_person = :id');
        $stmt->bindValue(':id', $data['idP']);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('adresy'));
})->setName('move_out');
