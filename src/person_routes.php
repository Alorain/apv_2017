<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    try {
        $parametry = $request->getQueryParams();
        if (empty($parametry['filtr'])) { // shoduje se s atributem name v index.latte
            //nezadanil nic => vracime vse
            $stmt = $this->db->prepare('SELECT p.*, l.*, pk.poc_kon, pm.poc_sch 
                                FROM person AS p
                                LEFT JOIN 
                                (
                                  SELECT id_person, COUNT(*) as poc_kon FROM contact
                                  GROUP BY id_person 
                                ) AS pk USING (id_person)
                                LEFT JOIN (
                                  SELECT id_person,COUNT(*) as poc_sch FROM person_meeting
                                  GROUP BY id_person
                                ) AS pm USING (id_person)
                                LEFT JOIN location AS l USING(id_location) ORDER BY last_name
                                LIMIT 10 OFFSET :o'); // ziskame vysledek
            // getQueryParam vrací zadaný parametr nebo má i implicitní hodnotu
            $stmt->bindValue(':o', 10 * $request->getQueryParam('offset', 0));
            $stmt->execute();

            $stmt2 = $this->db->query('SELECT COUNT(*) AS pocet FROM person');
            $info = $stmt2->fetch();
            $tplVars['pocet'] = ceil($info['pocet'] / 10); // Protože máme limit 10. Pocet kvuli aliasu v query
        } else {
            //filtruj!!
            $stmt = $this->db->prepare('SELECT p.*, l.*, pk.poc_kon, pm.poc_sch 
                                FROM person AS p
                                LEFT JOIN 
                                (
                                  SELECT id_person, COUNT(*) as poc_kon FROM contact
                                  GROUP BY id_person 
                                ) AS pk USING (id_person)
                                LEFT JOIN (
                                  SELECT id_person,COUNT(*) as poc_sch FROM person_meeting
                                  GROUP BY id_person
                                ) AS pm USING (id_person)
                                LEFT JOIN location AS l USING(id_location) WHERE first_name LIKE :f OR last_name LIKE :f 
                                ORDER BY last_name');
            // :f je zastupny symbol
            $stmt->bindValue(':f', '%' . $parametry['filtr'] . '%');
            $stmt->execute();
        }
        $tplVars['osoby'] = $stmt->fetchAll(); // z vysledku vytahneme data
        /* $this->view->render($response, 'index.latte', [ // provolame vyrenderovani sablony
          'osoby' => $data // predani promenne data pod nazvem "osoby"
          ]); */
        $this->view->render($response, 'index.latte', $tplVars);
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        //exit('Chyba DB, zkuste pozdeji');
        exit($ex->getMessage());
    }
})->setName('index');

$app->post('/smazat', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare(
                'DELETE FROM person WHERE id_person=:id'
        );
        $stmt->bindValue(':id', $data['id']);
        $stmt->execute();
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('delete');

$app->get('/editace', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    if (empty($id)) {
        exit('Neni zadano ID osoby.');
    }
    try {
        $stmt = $this->db->prepare('SELECT * FROM person WHERE id_person = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $osoba = $stmt->fetch();
        if (!empty($osoba)) {
            $tplVars['osoba'] = [
                'jm' => $osoba['first_name'],
                'pr' => $osoba['last_name'],
                'pz' => $osoba['nickname'],
                'vy' => $osoba['height'],
                'dn' => $osoba['birth_day'],
                'po' => $osoba['gender'],
            ];
            return $this->view->render($response, 'edit-person.latte', $tplVars);
        } else {
            exit('Osoba nenalezana.');
        }
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('edit');

$app->post('/editace', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    if (empty($id)) {
        exit('Neni zadano ID osoby.');
    }
    $parametry = $request->getParsedBody();
    try {
        if (!empty($parametry['pz']) && !empty($parametry['jm']) && !empty($parametry['pr'])) {
            $vy = empty($parametry['vy']) ? null : $parametry['vy'];
            $dn = empty($parametry['dn']) ? null : $parametry['dn'];
            $stmt = $this->db->prepare('UPDATE person SET first_name = :jm, last_name = :pr, nickname = :pz, height = :vy, birth_day = :dn, gender = :po '
                    . 'WHERE id_person=:id');
            $stmt->bindValue(':jm', $parametry['jm']);
            $stmt->bindValue(':pr', $parametry['pr']);
            $stmt->bindValue(':pz', $parametry['pz']);
            $stmt->bindValue(':po', $parametry['po']);
            $stmt->bindValue(':vy', $vy);
            $stmt->bindValue(':dn', $dn);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            return $response->withHeader('Location', '.');
        } else {
            $tplVars['error'] = 'Zadejte povinne udaje.';
        }
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        //exit('Zavazna chyba aplikace');
        exit($ex->getMessage());
    }
});

$app->get('/new-person&adress', function (Request $request, Response $response, $args) {
    $tplVars['osoba'] = [
        'jm' => '',
        'pr' => '',
        'pz' => '',
        'vy' => '',
        'dn' => '',
        'po' => '',
        'me' => '',
        'ul' => '',
        'cd' => '',
        'psc' => '',
    ];
    return $this->view->render($response, 'new-person&adress.latte', $tplVars);
})->setName('add_person_address');

$app->post('/new-person&adress', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        if (!empty($data['jm']) && !empty($data['pr']) && !empty($data['pz'])) {
            // zacatek transakce
            $this->db->beginTransaction();
            $idAdr = null;
            // Zadal adresu?
            if (!empty($data['me'])) {
                // Zadal detaily adresy?
                $ul = empty($data['ul']) ? null : $data['ul'];
                $cd = empty($data['cd']) ? null : $data['cd'];
                $psc = empty($data['psc']) ? null : $data['psc'];
                // Uložíme adresu
                $stmt = $this->db->prepare('INSERT INTO location (city, street_name, street_number, zip) VALUES (:me, :ul, :cd, :psc)');
                $stmt->bindValue(':me', $data['me']);
                $stmt->bindValue(':ul', $ul);
                $stmt->bindValue(':cd', $cd);
                $stmt->bindValue(':psc', $psc);
                $stmt->execute();

                // zjistíme ID
                $idAdr = $this->db->lastInsertId('location_id_location_seq');
            }

            $vy = empty($data['vy']) ? null : $data['vy'];
            $dn = empty($data['dn']) ? null : $data['dn'];


            $stmt = $this->db->prepare('INSERT INTO person (id_location, first_name, last_name, nickname, height, birth_day, gender) VALUES (:ida, :jm, :pr, :pz, :vy, :dn, :po)');
            $stmt->bindValue(':jm', $data['jm']);
            $stmt->bindValue(':pr', $data['pr']);
            $stmt->bindValue(':pz', $data['pz']);
            $stmt->bindValue(':po', $data['po']);
            $stmt->bindValue(':vy', $vy);
            $stmt->bindValue(':dn', $dn);
            $stmt->bindValue(':ida', $idAdr);
            $stmt->execute();
            // potvrzení transakce
            $this->db->commit();
            return $response->withHeader('Location', '.');
            //vypis osob
        } else {
            //chyba
            $tplVars['error'] = 'Zadejte povinne udaje.';
        }
    } catch (Exception $ex) {
        //navrat transakce do původního stavu
        $this->db->rollback();
        if ($ex->getCode() == 23505) {
            $tplVars['error'] = 'Tato osoba uz existuje.';
        } elseif ($ex->getCode() == 22007) {
            $tplVars['error'] = 'Spatny format data.';
        } else {
            $this->logger->error($ex->getMessage());
            //exit('Zavazna chyba aplikace');
            exit($ex->getMessage());
        }
    }
    $tplVars['osoba'] = $data;
    return $this->view->render($response, 'new-person&adress.latte', $tplVars);
});

$app->get('/profile', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try {
        $stmt = $this->db->prepare('SELECT * FROM person 
                                        LEFT JOIN location USING (id_location)  
                                        WHERE id_person = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $osoba = $stmt->fetch();

        $stmt2 = $this->db->prepare('SELECT p1.id_person as id1, p1.first_name as fn1, p1.last_name as ln1, p2.id_person as id2, p2.first_name as fn2,
                                            p2.last_name as ln2, id_relation_type, id_relation, czech FROM person AS p1
                                        JOIN relation ON p1.id_person = id_person1
                                        JOIN person AS p2 ON id_person2 = p2.id_person
                                        JOIN relation_type USING (id_relation_type)
                                        WHERE (p1.id_person = :id OR p2.id_person = :id) 
                                        ORDER BY id_relation_type');
        $stmt2->bindValue(':id', $id);
        $stmt2->execute();
        $vztahy = $stmt2->fetchAll();

        $stmt3 = $this->db->prepare('SELECT * FROM person_meeting
                                        JOIN meeting USING (id_meeting)
                                        JOIN location USING (id_location)
                                        WHERE id_person = :id
                                        ORDER BY start');
        $stmt3->bindValue(':id', $id);
        $stmt3->execute();
        $schuzky = $stmt3->fetchAll();

        $stmt4 = $this->db->prepare('SELECT * FROM contact JOIN contact_type USING (id_contact_type) WHERE id_person = :id');
        $stmt4->bindValue(':id', $id);
        $stmt4->execute();
        $kontakty = $stmt4->fetchAll();

        if (!empty($osoba)) {
            $tplVars['osoba'] = $osoba;
            $tplVars['vztahy'] = $vztahy;
            $tplVars['schuzky'] = $schuzky;
            $tplVars['kontakty'] = $kontakty;
            return $this->view->render($response, 'profile.latte', $tplVars);
        } else {
            exit('Osoba nenalezena.');
        }
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('profile');
