<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/relations', function (Request $request, Response $response, $args) {
    try {
        $parametry = $request->getQueryParams();
        if (empty($parametry['filtr']) && empty($parametry['type'])) {
            $stmt = $this->db->prepare('SELECT  p1.id_person AS id1, p1.first_name AS fn1, p1.last_name AS ln1, id_relation, id_relation_type, p2.id_person AS id2,
                                            p2.first_name AS fn2, p2.last_name AS ln2, czech FROM person AS p1
                                JOIN relation ON p1.id_person = id_person1
                                JOIN person AS p2 ON id_person2 = p2.id_person
                                JOIN relation_type USING (id_relation_type)
                                ORDER BY id_relation
                                LIMIT 10 OFFSET :o');
            $stmt->bindValue(':o', 10 * $request->getQueryParam('offset', 0));
            $stmt->execute();
            $vztahy = $stmt->fetchAll();
            $stmt2 = $this->db->query('SELECT COUNT(*) AS pocet FROM relation');
            $info = $stmt2->fetch();
            $tplVars['pocet'] = ceil($info['pocet'] / 10);
            $tplVars['vztahy'] = $vztahy;
        } else if (empty($parametry['filtr'])) {
            $stmt = $this->db->prepare('SELECT  p1.id_person AS id1, p1.first_name AS fn1, p1.last_name AS ln1, id_relation, id_relation_type, p2.id_person AS id2,
                                            p2.first_name AS fn2, p2.last_name AS ln2, czech FROM person AS p1
                                JOIN relation ON p1.id_person = id_person1
                                JOIN person AS p2 ON id_person2 = p2.id_person
                                JOIN relation_type USING (id_relation_type)
                                WHERE id_relation_type = :typ
                                ORDER BY id_relation
                                LIMIT 10 OFFSET :o');
            $stmt->bindValue(':o', 10 * $request->getQueryParam('offset', 0));
            $stmt->bindValue(':typ', $parametry['type']);
            $stmt->execute();
            $vztahy = $stmt->fetchAll();
            $stmt2 = $this->db->prepare('SELECT COUNT(*) AS pocet FROM relation WHERE id_relation_type = :typ');
            $stmt2->bindValue(':typ', $parametry['type']);
            $stmt2->execute();
            $info = $stmt2->fetch();
            $tplVars['pocet'] = ceil($info['pocet'] / 10);
            $tplVars['vztahy'] = $vztahy;
        } else {
            $stmt = $this->db->prepare('SELECT  p1.id_person AS id1, p1.first_name AS fn1, p1.last_name AS ln1, id_relation, id_relation_type, p2.id_person AS id2,
                                            p2.first_name AS fn2, p2.last_name AS ln2, czech FROM person AS p1
                                JOIN relation ON p1.id_person = id_person1
                                JOIN person AS p2 ON id_person2 = p2.id_person
                                JOIN relation_type USING (id_relation_type)
                                WHERE p1.first_name LIKE :para OR p1.last_name LIKE :para OR p2.first_name LIKE :para OR p2.last_name LIKE :para
                                ORDER BY id_relation');
            $stmt->bindValue(':para', '%' . $parametry['filtr'] . '%');
            $stmt->execute();
            $vztahy = $stmt->fetchAll();
            $tplVars['vztahy'] = $vztahy;
        }
        $stmt = $this->db->query('SELECT * FROM relation_type ORDER BY id_relation_type');
        $typy = $stmt->fetchAll();
        $tplVars['typy'] = $typy;
        return $this->view->render($response, 'print_relations.latte', $tplVars);
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('print_relations');

$app->get('/edit_relation', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    if (empty($id)) {
        exit('Neni zadano ID vztahu.');
    }
    try {
        $stmt = $this->db->prepare('SELECT  p1.id_person AS id1, p1.first_name AS fn1, p1.last_name AS ln1, id_relation, id_relation_type, p2.id_person AS id2, 
                                            p2.first_name AS fn2, p2.last_name AS ln2, czech FROM person AS p1
                                    JOIN relation ON p1.id_person = id_person1
                                    JOIN person AS p2 ON id_person2 = p2.id_person
                                    JOIN relation_type USING (id_relation_type)
                                    WHERE id_relation = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $vztah = $stmt->fetch();
        $tplVars['vztah'] = $vztah;
        $stmt2 = $this->db->query('SELECT * FROM relation_type');
        $typy = $stmt2->fetchAll();
        $tplVars['typy'] = $typy;
        return $this->view->render($response, 'edit_relation.latte', $tplVars);
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
})->setName('edit_relation');

$app->post('/edit_relation', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    if (empty($id)) {
        exit('Neni zadano ID schůzky.');
    }
    $parametry = $request->getParsedBody();

    try {
        $stmt = $this->db->prepare('UPDATE relation SET id_relation_type = :type '
                . 'WHERE id_relation = :id');
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':type', $parametry['type']);
        $stmt->execute();
        return $response->withHeader('Location', $this->router->pathFor('print_relations'));
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        //exit('Zavazna chyba aplikace');
        exit($ex->getMessage());
    }
});

$app->get('/new_relation', function (Request $request, Response $response, $args) {
    $stmt = $this->db->query('SELECT * FROM person ORDER BY last_name');
    $osoby = $stmt->fetchAll();
    $stmt2 = $this->db->query('SELECT * FROM relation_type');
    $typy = $stmt2->fetchAll();
    $tplVars['osoby'] = $osoby;
    $tplVars['typy'] = $typy;
    return $this->view->render($response, 'new_relation.latte', $tplVars);
})->setName('new_relation');

$app->post('/new_relation', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        if ($data['id1'] != $data['id2']) {
            $desc = empty($data['desc']) ? '' : $data['desc'];
            $stmt = $this->db->prepare('INSERT INTO relation (id_person1, id_person2, description, id_relation_type)
                                        VALUES (:id1, :id2, :desc, :type)');
            $stmt->bindValue(':id1', $data['id1']);
            $stmt->bindValue(':id2', $data['id2']);
            $stmt->bindValue(':desc', $desc);
            $stmt->bindValue(':type', $data['type']);
            $stmt->execute();
            return $response->withHeader('Location', $this->router->pathFor('print_relations'));
        } else {
            $tplVars['error'] = "Osoba nemůže mít vztah sama se sebou!";
        }
    } catch (Exception $ex) {
        if ($ex->getCode() == 23505) {
            $tplVars['error'] = 'Tento vztah už existuje.';
        } else {
            $this->logger->error($ex->getMessage());
            exit($ex->getMessage());
        }
    }
    $stmt = $this->db->query('SELECT * FROM person ORDER BY last_name');
    $osoby = $stmt->fetchAll();
    $stmt2 = $this->db->query('SELECT * FROM relation_type');
    $typy = $stmt2->fetchAll();
    $tplVars['osoby'] = $osoby;
    $tplVars['typy'] = $typy;

    return $this->view->render($response, 'new_relation.latte', $tplVars);
});

$app->post('/delete_relation', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare(
                'DELETE FROM relation WHERE id_relation = :id'
        );
        $stmt->bindValue(':id', $data['id']);
        $stmt->execute();
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('print_relations'));
})->setName('delete_relation');
