<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->group('/auth', function() use ($app) {

    include 'person_routes.php';

    include 'location_routes.php';

    include 'meeting_routes.php';

    include 'relation_routes.php';

    include 'contact_routes.php';

    $app->get('/logout', function (Request $request, Response $response, $args) {
        session_destroy();
        return $response->withHeader('Location', $this->router->pathFor('login'));
    })->setName('logout');
})->add(function($request, $response, $next) {
    // middleware 
    if (!empty($_SESSION['user'])) {
        return $next($request, $response);
    } else {
        //neni přihlášený!
        return $response->withHeader('Location', $this->router->pathFor('login'));
    }
});

include 'log_reg.php';

$app->get('/', function (Request $request, Response $response, $args) {
    return $response->withHeader('Location', $this->router->pathFor('login'));
});


