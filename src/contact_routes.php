<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/print_contacts', function (Request $request, Response $response, $args) {
    try {
        $parametry = $request->getQueryParams();
        if (empty($parametry['filtr']) && empty($parametry['type'])) {
            $stmt = $this->db->prepare('SELECT * FROM contact
                                        JOIN person USING (id_person)
                                        JOIN contact_type USING (id_contact_type)
                                        ORDER BY last_name
                                        LIMIT 10 OFFSET :o');
            $stmt->bindValue(':o', 10 * $request->getQueryParam('offset', 0));
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt2 = $this->db->query('SELECT COUNT(*) AS pocet FROM contact');
            $info = $stmt2->fetch();
            $tplVars['data'] = $data;
            $tplVars['pocet'] = ceil($info['pocet'] / 10);
        } else if (empty($parametry['filtr'])) {
            $stmt = $this->db->prepare('SELECT * FROM contact
                                        JOIN person USING (id_person)
                                        JOIN contact_type USING (id_contact_type)
                                        WHERE id_contact_type = :typ
                                        ORDER BY last_name
                                        LIMIT 10 OFFSET :o');
            $stmt->bindValue(':typ', $parametry['type']);
            $stmt->bindValue(':o', 10 * $request->getQueryParam('offset', 0));
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt2 = $this->db->prepare('SELECT COUNT(*) AS pocet FROM contact WHERE id_contact_type = :typ');
            $stmt2->bindValue(':typ', $parametry['type']);
            $stmt2->execute();
            $info = $stmt2->fetch();
            $tplVars['data'] = $data;
            $tplVars['pocet'] = ceil($info['pocet'] / 10);
        } else {
            $stmt = $this->db->prepare('SELECT * FROM contact
                                        JOIN person USING (id_person)
                                        JOIN contact_type USING (id_contact_type)
                                        WHERE first_name LIKE :f OR last_name LIKE :f OR contact LIKE :f');
            $stmt->bindValue(':f', '%' . $parametry['filtr'] . '%');
            $stmt->execute();
            $data = $stmt->fetchAll();
            $tplVars['data'] = $data;
        }
        $stmt = $this->db->query('SELECT * FROM contact_type ORDER BY id_contact_type');
        $typy = $stmt->fetchAll();
        $tplVars['typy'] = $typy;
        $this->view->render($response, 'print_contacts.latte', $tplVars);
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
})->setName('print_contacts');

$app->get('/edit_contacts', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try {
        $stmt = $this->db->prepare('SELECT * FROM contact
                                    JOIN contact_type USING (id_contact_type)
                                    WHERE id_person = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $kontakty = $stmt->fetchAll();
        $stmt = $this->db->query('SELECT * FROM contact_type ORDER BY id_contact_type');
        $typy = $stmt->fetchAll();
        $tplVars['typy'] = $typy;
        $tplVars['kontakty'] = $kontakty;
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
    $this->view->render($response, 'edit_contact.latte', $tplVars);
})->setName('edit_contact');

$app->post('/edit_contacts', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        // facebook, skype, tel, email, jabber, web 
        $this->db->beginTransaction();
        $skype = empty($data['skype']) ? NULL : $data['skype'];
        $jabber = empty($data['jabber']) ? NULL : $data['jabber'];
        $tel = NULL;
        if (!empty($data['tel'])) {
            if (is_numeric($data['tel'])) {
                $tel = $data['tel'];
            }
        }
        $web = empty($data['web']) ? NULL : $data['web'];
        $facebook = empty($data['facebook']) ? NULL : $data['facebook'];
        $email = empty($data['email']) ? NULL : $data['email'];

        $kontakty = array($facebook, $skype, $tel, $email, $jabber, $web);


        $stmt = $this->db->prepare('DELETE FROM contact WHERE id_person = :id');
        $stmt->bindValue(':id', $data['idP']);
        $stmt->execute();

        $stmt2 = $this->db->query('SELECT COUNT(*) FROM contact_type');
        $pocet = $stmt2->fetch();

        for ($x = 0; $x < $pocet['count']; $x++) {
            if ($kontakty[$x] != NULL) {
                $stmt = $this->db->prepare('INSERT INTO contact (id_person, id_contact_type, contact)
                                        VALUES (:idP, :typ, :kon)');
                $stmt->bindValue(':idP', $data['idP']);
                $stmt->bindValue(':typ', $x + 1);
                $stmt->bindValue(':kon', $kontakty[$x]);
                $stmt->execute();
                echo "vloženo ";
            }
        }
        $this->db->commit();
        return $response->withHeader('Location', $this->router->pathFor('print_contacts'));
    } catch (Exception $ex) {
        $this->db->rollback();
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
});

$app->get('/new_contact', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try {
        $stmt = $this->db->query('SELECT * FROM person ORDER BY last_name');
        $kontakty = $stmt->fetchAll();
        $stmt = $this->db->query('SELECT * FROM contact_type ORDER BY id_contact_type');
        $typy = $stmt->fetchAll();
        $tplVars['typy'] = $typy;
        $tplVars['osoby'] = $kontakty;
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
    $this->view->render($response, 'new_contact.latte', $tplVars);
})->setName('new_contact');

$app->post('/new_contact', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        // facebook, skype, tel, email, jabber, web 
        $this->db->beginTransaction();
        $skype = empty($data['skype']) ? NULL : $data['skype'];
        $jabber = empty($data['jabber']) ? NULL : $data['jabber'];
        $tel = NULL;
        if (!empty($data['tel'])) {
            if (is_numeric($data['tel'])) {
                $tel = $data['tel'];
            }
        }
        $web = empty($data['web']) ? NULL : $data['web'];
        $facebook = empty($data['facebook']) ? NULL : $data['facebook'];
        $email = empty($data['email']) ? NULL : $data['email'];

        $kontakty = array($facebook, $skype, $tel, $email, $jabber, $web);


        $stmt = $this->db->prepare('DELETE FROM contact WHERE id_person = :id');
        $stmt->bindValue(':id', $data['idP']);
        $stmt->execute();

        $stmt2 = $this->db->query('SELECT COUNT(*) FROM contact_type');
        $pocet = $stmt2->fetch();

        for ($x = 0; $x < $pocet['count']; $x++) {
            if ($kontakty[$x] != NULL) {
                $stmt = $this->db->prepare('INSERT INTO contact (id_person, id_contact_type, contact)
                                        VALUES (:idP, :typ, :kon)');
                $stmt->bindValue(':idP', $data['idP']);
                $stmt->bindValue(':typ', $x + 1);
                $stmt->bindValue(':kon', $kontakty[$x]);
                $stmt->execute();
                echo "vloženo ";
            }
        }
        $this->db->commit();
        return $response->withHeader('Location', $this->router->pathFor('print_contacts'));
    } catch (Exception $ex) {
        $this->db->rollback();
        $this->logger->error($ex->getMessage());
        exit($ex->getMessage());
    }
});

$app->post('/delete_contact', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare(
                'DELETE FROM contact WHERE id_contact = :id'
        );
        $stmt->bindValue(':id', $data['id']);
        $stmt->execute();
    } catch (Exception $ex) {
        exit($ex->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('print_contacts'));
})->setName('delete_contact');